from discord.ext import commands
import config


def is_owner():
    def predicate(ctx):
        return ctx.author.id == int(config.owner_id)
        raise commands.CheckFailure("Only the bot owner may run this command.")
    return commands.check(predicate)

