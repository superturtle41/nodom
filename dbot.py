import discord
from discord.ext import commands
import logging
import config
import sys
import traceback
import redis
from datetime import datetime

description = 'Bot written by Dr Turtle'
COGS = ['cogs.basic', 'cogs.embed', 'cogs.admin']


def get_prefix(client, message):
    prefix = [config.prefix]

    return commands.when_mentioned_or(*prefix)(client, message)


class Nodom(commands.Bot):
    def __init__(self, command_prefix, desc=None, **options):
        super(Nodom, self).__init__(command_prefix, descrirption=desc, **options)
        self.redis = redis.Redis(host='redis', port=6379, db=0, decode_responses=True)

    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.CommandNotFound):
            return

        elif isinstance(error, commands.CheckFailure):
            msg = str(error) or "You are not allowed to run this command."
            return await ctx.send(f"Error: {msg}")

        elif isinstance(error, commands.MissingRequiredArgument):
            msg = str(error) or "Missing Unknown Required Argument"
            return await ctx.send(f"Error: {msg}")

        elif isinstance(error, commands.CommandInvokeError):
            msg = str(error) or "Command raised an exception."
            return await ctx.send(f"Error: {msg}")

        log.warning("Error caused by message: `{}`".format(ctx.message.content))
        for line in traceback.format_exception(type(error), error, error.__traceback__):
            log.warning(line)


log_formatter = logging.Formatter('%(levelname)s:%(name)s: %(message)s')
handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(log_formatter)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logger.addHandler(handler)
log = logging.getLogger('bot')

logging.basicConfig(level=logging.WARN)

bot = Nodom(command_prefix=get_prefix, desc=description, case_insensitive=True)


@bot.event
async def on_ready():
    log.info(f'Logged in as {bot.user.name} - {bot.user.id}')
    bot.remove_command('help')
    for cog in COGS:
        bot.load_extension(cog)


if __name__ == "__main__":
    bot.run(config.token, reconnect=True, bot=True)
