import discord.ext.test as dpytest
from discord.ext import commands
import dbot
import pytest


@pytest.fixture
def nodom_bot():
    bot = dbot.Nodom(dbot.get_prefix)
    bot.remove_command("help")
    for cog in dbot.COGS:
        bot.load_extension(cog)
    return bot

# BASIC TESTING!


@pytest.mark.asyncio
async def test_say(nodom_bot):
    bot = nodom_bot
    # Load any extensions/cogs you want to in here
    prefix = dbot.config.prefix

    dpytest.configure(bot)

    await dpytest.message(prefix+"say hello")
    dpytest.verify_message("**hello**")

    await dpytest.message(prefix+'say hello world')
    dpytest.verify_message('**hello world**')


@pytest.mark.asyncio
async def test_ping(nodom_bot):
    bot = nodom_bot
    prefix = dbot.config.prefix
    dpytest.configure(bot)

    await dpytest.message(prefix+"ping")
    dpytest.verify_message("Pong.")

# ADMIN TESTING!


@pytest.mark.asyncio
async def test_eval(nodom_bot):
    bot = nodom_bot
    prefix = dbot.config.prefix
    dpytest.configure(bot)
    owner = dpytest.backend.make_user("Dr Turtle", 1030, None, int(dbot.config.owner_id))
    dpytest.get_config()[3].append(owner)

    await dpytest.message(prefix + "eval 1==1", 0, 1)
    dpytest.verify_message("True")
