FROM python:3.7-stretch

ARG DBOT_ARGS
ARG ENVIRONMENT=production
ARG COMMIT=""

RUN useradd --create-home nodom
USER nodom
WORKDIR /home/nodom

ENV GIT_COMMIT_SHA=${COMMIT}

COPY --chown=nodom:nodom requirements.txt .
RUN pip install --user --no-warn-script-location -r requirements.txt

COPY --chown=nodom:nodom dbot.py .
COPY --chown=nodom:nodom . .

#COPY --chown=nodom:nodom docker/credentials-${ENVIRONMENT}.py credentials.py

# Download AWS pubkey to connect to documentDB
#RUN if [ "$ENVIRONMENT" = "production" ]; then wget https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem; fi

ENTRYPOINT python -u dbot.py $DBOT_ARGS