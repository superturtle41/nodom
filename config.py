import os
token = os.environ['DISCORD_BOT_TOKEN']
prefix = os.environ['DISCORD_PREFIX']
owner_id = os.environ['DISCORD_OWNER_USER_ID']