from discord.ext import commands
from datetime import datetime, timedelta
from math import floor, isnan


class Basic(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="ping", description="Checks bot response time.", aliases=['p'])
    async def ping(self, ctx):
        now = datetime.utcnow()
        pong = await ctx.send("Pong.")
        if self.bot.latency == float('inf'):
            await pong.edit(content="Infinite Latency Detected. Houston, we have a problem.")
            return
        delta = datetime.utcnow() - now
        httping = floor(delta.total_seconds() * 1000)
        wsping = floor(self.bot.latency * 1000) if not isnan(self.bot.latency) else "Unknown"
        await pong.edit(content=f"Pong.\nHTTP Ping = {httping} ms.\nWS Ping = {wsping} ms.")

    @commands.command(name="say", description="Repeats what you send it.", aliases=['echo'])
    async def say(self, ctx, *, msg):
        await ctx.send(f"**{msg.strip()}**")


def setup(bot):
    bot.add_cog(Basic(bot))
